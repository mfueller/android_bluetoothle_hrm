/*
 * Copyright (C) 2013 The Android Open Source Project
 * This software is based on Apache-licensed code from the above.
 * 
 * Copyright (C) 2013 APUS
 *
 *     This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.

 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package brsu.ble;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import brsu.ble.SampleGattAttributes;

//import pro.apus.heartrate.R;

/**
 * Service for managing connection and data communication with a GATT server
 * hosted on a given Bluetooth LE device.
 */
public class BluetoothLeService extends Service {
	private static final int _NO_HBM_TIME_IN_MS = 20/*s*/ * 1000;
	private static final int _NO_HBM_CHECH_INTERVAL_IN_MS = 10/*s*/ * 1000;

	private final static String TAG = BluetoothLeService.class.getSimpleName();

	private BluetoothManager mBluetoothManager;
	private BluetoothAdapter mBluetoothAdapter;
	private String mBluetoothDeviceAddress;
	private BluetoothGatt mBluetoothGatt;
	private int mConnectionState = STATE_DISCONNECTED;

	private Builder BLEnotificationBuilder;

	private static final int STATE_DISCONNECTED = 0;
	private static final int STATE_CONNECTING = 1;
	private static final int STATE_CONNECTED = 2;

	
	public final static String ACTION_GATT_CONNECTED = "brsu.ble.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTED = "brsu.ble.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_GATT_SERVICES_DISCOVERED = "brsu.ble.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_DATA_AVAILABLE = "brsu.ble.heartrate.AVAILABLE";
	public final static String BLE_ADRESS_SELECTED = "brsu.ble.address";
	public final static String BLE_ACTION_FINISH = "brsu.ble.finish";
	
	
	public final static String BC_INTEND_ACTION = "brsu.ble.heartrate";
	public final static String BC_INTEND_RR_ACTION = "brsu.ble.rr";
	
	public final static String EXTRA_DATA = "brsu.ble.heartrate";
	public final static String RR_EXTRA_DATA = "brsu.ble.rr";
	public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
	public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
	
	public final static UUID UUID_HEART_RATE_MEASUREMENT = UUID
			.fromString(SampleGattAttributes.HEART_RATE_MEASUREMENT);

	private NotificationManager mNotifyManager;

	private int notificationID = 1;

	private Intent receiver;

	Timer timer = new Timer();
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		
		//Intent notificationIntent = new Intent(this, BluetoothLeService.class);
		Intent closeIntent = new Intent(BLE_ACTION_FINISH);
		
		Intent openScanIntent = new Intent(this, DeviceScanActivity.class);
		openScanIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0, closeIntent,PendingIntent.FLAG_CANCEL_CURRENT); 
		PendingIntent pendingOpenScanIntent = PendingIntent.getActivity(this,	0, openScanIntent, 0);
				
				//PendingIntent.getActivity(this, 0,
				//closeIntent, 0);
		
		
		BLEnotificationBuilder = new Notification.Builder(this)
				.setContentTitle("BLE Connection Service")
				.setContentText("[hbm]")
				.setSmallIcon(R.drawable.ic_launcher)
				.addAction(R.drawable.ic_launcher, "stop service", pendingIntent)
				.addAction(R.drawable.ic_launcher, "scan device", pendingOpenScanIntent);
		
		this.startForeground(notificationID, BLEnotificationBuilder.build());

		final IntentFilter intentFilter = new IntentFilter();

		intentFilter.addAction(BLE_ADRESS_SELECTED);
		intentFilter.addAction(BLE_ACTION_FINISH);

		// bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
		receiver = this.registerReceiver(mSelectDeviceReceiver, intentFilter);

		this.initialize();

		timer.cancel();
		timer.purge();
		timer = new Timer();
		
		timer.scheduleAtFixedRate(new CheckDeadHbmTask(), 0, _NO_HBM_CHECH_INTERVAL_IN_MS);
		
		return Service.START_NOT_STICKY;
	}

	// Implements callback methods for GATT events that the app cares about. For
	// example,
	// connection change and services discovered.
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			String intentAction;
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				intentAction = ACTION_GATT_CONNECTED;
				mConnectionState = STATE_CONNECTED;
				broadcastUpdate(intentAction);
				Log.i(TAG, "Connected to GATT server.");
				// Attempts to discover services after successful connection.
				Log.i(TAG, "Attempting to start service discovery:"
						+ mBluetoothGatt.discoverServices());

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				intentAction = ACTION_GATT_DISCONNECTED;
				mConnectionState = STATE_DISCONNECTED;
				Log.i(TAG, "Disconnected from GATT server.");
				broadcastUpdate(intentAction);
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
				displayGattServices(BluetoothLeService.this
						.getSupportedGattServices());
			} else {
				Log.w(TAG, "onServicesDiscovered received: " + status);
			}
		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
		}
	};

	private long lastHbmUpdate;

	private void broadcastUpdate(final String action) {
		final Intent intent = new Intent(action);
		sendBroadcast(intent);
	}

	private void broadcastHeartBeat(String action, int hbm) {
		final Intent intent = new Intent(action);
		//intent.addFlags(flags)
		intent.putExtra(EXTRA_DATA, hbm);
		BLEnotificationBuilder.setContentText("" + hbm + " hbm");
		this.mNotifyManager.notify(notificationID, BLEnotificationBuilder.build());
		this.sendBroadcast(intent);
		if (hbm > 0) {
			lastHbmUpdate = System.currentTimeMillis();
		}
	}
	
	private long lastRRUpdate;

	private void broadcastRR(String action, int[] rr_in_ms) {
		final Intent intent = new Intent(action);
		//intent.addFlags(flags)
		intent.putExtra(RR_EXTRA_DATA, rr_in_ms);
		//BLEnotificationBuilder.setContentText("" + rr_in_ms + " hbm");
		//this.mNotifyManager.notify(notificationID, BLEnotificationBuilder.build());
		this.sendBroadcast(intent);
		if (rr_in_ms[0] > 0) {
			lastRRUpdate = System.currentTimeMillis();
		}
	}
	
	private class CheckDeadHbmTask extends TimerTask
    { 
        public void run() 
        {
            checkDeadHbmHandler.sendEmptyMessage(0);
        }
    }    

	
	private final Handler checkDeadHbmHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            //Toast.makeText(getApplicationContext(), "test", Toast.LENGTH_SHORT).show();
        	if (lastHbmUpdate + _NO_HBM_TIME_IN_MS < System.currentTimeMillis()) {
        		broadcastHeartBeat(BC_INTEND_ACTION, -1);
        		Log.e(TAG, "Last heart beat data too old - not connection to sensor?");
        	}
        }
    }; 
    
	private void broadcastUpdate(final String action,
			final BluetoothGattCharacteristic characteristic) {
		
		// This is special handling for the Heart Rate Measurement profile. Data
		// parsing is
		// carried out as per profile specifications:
		// http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
		if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
			int flag = characteristic.getValue()[0];
			int format = -1;
			int pointer = 0;
			if ((flag & 0x01) != 0) {
				format = BluetoothGattCharacteristic.FORMAT_UINT16;
				Log.d(TAG, "Heart rate format UINT16.");
				pointer += 1;
			} else {
				format = BluetoothGattCharacteristic.FORMAT_UINT8;
				Log.d(TAG, "Heart rate format UINT8.");
				pointer += 2;
			}
			final int heartRate = characteristic.getIntValue(format, 1);
			Log.d(TAG, String.format("Received heart rate: %d", heartRate));
			
			broadcastHeartBeat(BC_INTEND_ACTION, heartRate);
			
			if ((flag & 0x02) != 0) {
				//pointer++;
			}
				
			if ((flag & 0x04) != 0) { // RR value
				int[] rr_values = new int[]{0,0,0,0};
				int i=0;
				while(characteristic.getValue().length > pointer + 1) {
					int rr_value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, pointer);
					Log.d(TAG, String.format("RR value: %d", rr_value));
					pointer += 2;
					
					rr_values[i] = rr_value;
					i++;
				}
				broadcastRR(BC_INTEND_RR_ACTION, rr_values);
			} else {
				Log.d(TAG, String.format("No RR value received"));
				broadcastRR(BC_INTEND_RR_ACTION, new int[]{0,0,0,0});
			}
			
		} else {
			final Intent intent = new Intent(action);

			// For all other profiles, writes the data formatted in HEX.
			final byte[] data = characteristic.getValue();
			if (data != null && data.length > 0) {
				final StringBuilder stringBuilder = new StringBuilder(
						data.length);
				for (byte byteChar : data)
					stringBuilder.append(String.format("%02X ", byteChar));
				intent.putExtra(EXTRA_DATA, new String(data) + "\n"
						+ stringBuilder.toString());
			}
			sendBroadcast(intent);
		}
		
	}

	public class LocalBinder extends Binder {
		BluetoothLeService getService() {
			return BluetoothLeService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		// After using a given device, you should make sure that
		// BluetoothGatt.close() is called
		// such that resources are cleaned up properly. In this particular
		// example, close() is
		// invoked when the UI is disconnected from the Service.
		close();
		return super.onUnbind(intent);
	}

	private final IBinder mBinder = new LocalBinder();

	/**
	 * Initializes a reference to the local Bluetooth adapter.
	 * 
	 * @return Return true if the initialization is successful.
	 */
	public boolean initialize() {
		// For API level 18 and above, get a reference to BluetoothAdapter
		// through
		// BluetoothManager.
		if (mBluetoothManager == null) {
			mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			if (mBluetoothManager == null) {
				Log.e(TAG, "Unable to initialize BluetoothManager.");
				return false;
			}
		}

		mBluetoothAdapter = mBluetoothManager.getAdapter();
		if (mBluetoothAdapter == null) {
			Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
			return false;
		}

		return true;
	}

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 * 
	 * @param address
	 *            The device address of the destination device.
	 * 
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */
	public boolean connect(final String address) {
		if (mBluetoothAdapter == null || address == null) {
			Log.w(TAG,
					"BluetoothAdapter not initialized or unspecified address.");
			return false;
		}

		// Previously connected device. Try to reconnect.
		if (mBluetoothDeviceAddress != null
				&& address.equals(mBluetoothDeviceAddress)
				&& mBluetoothGatt != null) {
			Log.d(TAG,
					"Trying to use an existing mBluetoothGatt for connection.");
			if (mBluetoothGatt.connect()) {
				mConnectionState = STATE_CONNECTING;
				return true;
			} else {
				return false;
			}
		}

		final BluetoothDevice device = mBluetoothAdapter
				.getRemoteDevice(address);
		if (device == null) {
			Log.w(TAG, "Device not found.  Unable to connect.");
			return false;
		}
		// We want to directly connect to the device, so we are setting the
		// autoConnect
		// parameter to false.
		mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
		Log.d(TAG, "Trying to create a new connection.");
		mBluetoothDeviceAddress = address;
		mConnectionState = STATE_CONNECTING;
		return true;
	}

	/**
	 * Disconnects an existing connection or cancel a pending connection. The
	 * disconnection result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 * callback.
	 */
	public void disconnect() {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.disconnect();
	}

	/**
	 * After using a given BLE device, the app must call this method to ensure
	 * resources are released properly.
	 */
	public void close() {
		if (mBluetoothGatt == null) {
			return;
		}
		mBluetoothGatt.close();
		mBluetoothGatt = null;
	}

	/**
	 * Request a read on a given {@code BluetoothGattCharacteristic}. The read
	 * result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
	 * callback.
	 * 
	 * @param characteristic
	 *            The characteristic to read from.
	 */
	public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.readCharacteristic(characteristic);
	}

	/**
	 * Enables or disables notification on a give characteristic.
	 * 
	 * @param characteristic
	 *            Characteristic to act on.
	 * @param enabled
	 *            If true, enable notification. False otherwise.
	 */
	public void setCharacteristicNotification(
			BluetoothGattCharacteristic characteristic, boolean enabled) {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

		try {
			// This is specific to Heart Rate Measurement.
			if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
				BluetoothGattDescriptor descriptor = characteristic
						.getDescriptor(UUID
								.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
				descriptor
						.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
				mBluetoothGatt.writeDescriptor(descriptor);

			}
		} catch (Exception e) {
			Log.d(TAG,
					"Exception while setting up notification for heartrate.", e);
		}
	}

	/**
	 * Retrieves a list of supported GATT services on the connected device. This
	 * should be invoked only after {@code BluetoothGatt#discoverServices()}
	 * completes successfully.
	 * 
	 * @return A {@code List} of supported services.
	 */
	public List<BluetoothGattService> getSupportedGattServices() {
		if (mBluetoothGatt == null)
			return null;

		return mBluetoothGatt.getServices();
	}

	private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
	private final String LIST_NAME = "NAME";
	private final String LIST_UUID = "UUID";
	private BluetoothGattCharacteristic mNotifyCharacteristic;

	// Demonstrates how to iterate through the supported GATT
	// Services/Characteristics.
	// In this sample, we populate the data structure that is bound to the
	// ExpandableListView
	// on the UI.
	private void displayGattServices(List<BluetoothGattService> gattServices) {
		if (gattServices == null)
			return;
		String uuid = null;
		String unknownServiceString = getResources().getString(
				R.string.unknown_service);
		String unknownCharaString = getResources().getString(
				R.string.unknown_characteristic);
		ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
		ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData = new ArrayList<ArrayList<HashMap<String, String>>>();
		mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

		// Loops through available GATT Services.
		for (BluetoothGattService gattService : gattServices) {
			HashMap<String, String> currentServiceData = new HashMap<String, String>();
			uuid = gattService.getUuid().toString();
			currentServiceData.put(LIST_NAME,
					SampleGattAttributes.lookup(uuid, unknownServiceString));
			currentServiceData.put(LIST_UUID, uuid);
			gattServiceData.add(currentServiceData);

			ArrayList<HashMap<String, String>> gattCharacteristicGroupData = new ArrayList<HashMap<String, String>>();
			List<BluetoothGattCharacteristic> gattCharacteristics = gattService
					.getCharacteristics();
			ArrayList<BluetoothGattCharacteristic> charas = new ArrayList<BluetoothGattCharacteristic>();

			// Loops through available Characteristics.
			for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {

				if (UUID.fromString(SampleGattAttributes.HEART_RATE_MEASUREMENT)
						.equals(gattCharacteristic.getUuid())) {
					Log.d(TAG, "Found heart rate");
					mNotifyCharacteristic = gattCharacteristic;
					this.setCharacteristicNotification(mNotifyCharacteristic,
							true);
				}

				charas.add(gattCharacteristic);
				HashMap<String, String> currentCharaData = new HashMap<String, String>();
				uuid = gattCharacteristic.getUuid().toString();
				currentCharaData.put(LIST_NAME,
						SampleGattAttributes.lookup(uuid, unknownCharaString));
				currentCharaData.put(LIST_UUID, uuid);
				gattCharacteristicGroupData.add(currentCharaData);
			}
			mGattCharacteristics.add(charas);
			gattCharacteristicData.add(gattCharacteristicGroupData);
		}

	}


	private String mDeviceName;
	private String mDeviceAddress;

	private final BroadcastReceiver mSelectDeviceReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			if (BLE_ADRESS_SELECTED.equals(action)) {
				mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
				mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

				BluetoothLeService.this.connect(mDeviceAddress);
			}
			if (BLE_ACTION_FINISH.equals(action)) {
				//Log.e("BLE", "ble.address");
				timer.cancel();
				timer.purge();
				
				BluetoothLeService.this.close();
				BluetoothLeService.this.disconnect();
				BluetoothLeService.this.unregisterReceiver(mSelectDeviceReceiver);
				BluetoothLeService.this.stopSelf();
			}
		}
	};
	
	
	


}
